const userRepository = require("../repositories/userRepository");
const encrypt = require("../controllers/auth")
const jwt = require("jsonwebtoken");

module.exports = {
    async create(requestBody) {
        const email = requestBody.body.email;
        const pass = requestBody.body.password;
        const name = requestBody.body.name;
        const role = requestBody.body.role;
        const roleid = requestBody.body.roleid;
        
    
        const password = await encrypt.encryptPassword(pass);

        const data = userRepository.create({ email, password, name ,role, roleid});
        return {
            data
        }
    },

    async checkUser(req, res) {
          let checkIfExist = await userRepository.findOne({
            where: { email: req.body.email},
          });
      
          if (checkIfExist) {
            let idGenerator = checkIfExist.id;
            let validation = await userRepository.findOne({
              where: { id: idGenerator },
            });
            let checkPassword = await encrypt.decryptPass(
              validation.password,
              req.body.password
            );
            if (req.body.email === validation.email && checkPassword) {
              let user = {
                id: validation.id,
                email: validation.email,
                name: validation.name,
                role: validation.role,
                role_id: validation.roleId
              };
              let token = jwt.sign(user, "s3cr3t");
             console.log(user);
              return {
                data: user,
                token,
                message: 'User found'
            }
            } 
          } 
        } 

}