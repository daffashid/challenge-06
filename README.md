# Car Management API

Car Management API (Backend Only)

## Features

- ### Account Management

```
  Create Account
  Read Account
```

- ### Car Management

```
  Create Car
```

## How to Run (Installation)

```
npm install
npx sequelize-cli db:create
npx sequelize-cli db:migrate
npx sequelize-cli db:seed:all
npm start
```

## Tech

| Package       | Version | Source                                    |
| ------------- | ------- | ----------------------------------------- |
| bcryptjs      | ^5.0.1  | https://www.npmjs.com/package/bcryptjs    |
| express       | ^3.1.7  | https://expressjs.com/                    |
| jsonwebtoken  | ^8.5.1  | https://jwt.io/                           |
| pg            | ^8.7.3  | https://yarnpkg.com/package/pg            |
| pg-hstore     | ^2.3.4  | https://yarnpkg.com/package/pg-hstore     |
| sequelize     | ^6.19.0 | https://sequelize.org/                    |
| sequelize-cli | ^6.4.1  | https://yarnpkg.com/package/sequelize-cli |
