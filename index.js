const express = require("express");
const app = express();
// const swaggerUI = require("swagger-ui-express");
// const cors = require("cors");
const controller = require("./app/controllers");

// Define PORT
const { PORT = 8000 } = process.env;

app.use(express.json());

// Create account
app.post("/api/v1/register", controller.userController.register);
app.post("/api/v1/carpost", controller.carController.create)
app.post("/api/v1/login",controller.userController.login)

app.listen(PORT, () => {
  console.log("Server running on port : ", PORT);
});
