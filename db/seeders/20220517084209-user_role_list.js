'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('userroles', [{
      name: 'superadmin',
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      name: 'admin',
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      name: 'member',
      created_at: new Date(),
      updated_at: new Date()
    }
  ], {});
  
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkInsert('userroles', null, {});
  }
};
