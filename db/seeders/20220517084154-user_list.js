'use strict';
  const encrypt = require('../../app/controllers/auth')
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const encryptedPassword = await encrypt.encryptPassword('superadmin')
     await queryInterface.bulkInsert('users', [{
      email: 'superadmin@gmail.com',
      password: encryptedPassword,
      role: 'superadmin',
      name: 'daffa',
      roleId: 1,
      created_at: new Date(),
      updated_at: new Date()
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkInsert('users', null, {});
  }
};
